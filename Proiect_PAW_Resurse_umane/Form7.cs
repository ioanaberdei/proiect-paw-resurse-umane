﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_PAW_Resurse_umane
{
    public partial class Form7 : Form
    {
        private string optiune;
        public Form7()
        {
            InitializeComponent();
            comboBox1.Items.Add("P");
            comboBox1.Items.Add("N");
        }

        public string Optiune { get => optiune; set => optiune = value; }

        private void button1_Click(object sender, EventArgs e)
        {
            optiune = comboBox1.Text;
            Close();
        }
    }
}
