﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_PAW_Resurse_umane
{
    public partial class Form2 : Form
    {
       
        const string ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source= C:\Users\ioana\Desktop\AN 2\Sem 2\PAW\Proiect_PAW_Resurse_umane\Proiect_PAW_Resurse_umane\assets\bazadedate.mdb";
        const string ProviderName = @"System.Data.OleDb";
        public Form2()
        {
            InitializeComponent();
            listView1.View = View.Details;
            listView1.Columns.Add("Id", 30, HorizontalAlignment.Center);
            listView1.Columns.Add("Nume", 120, HorizontalAlignment.Center);
            listView1.Columns.Add("Prenume", 120, HorizontalAlignment.Center);
            listView1.Columns.Add("Nivel educatie", 100, HorizontalAlignment.Center);
            listView1.Columns.Add("Data angajarii", 130, HorizontalAlignment.Center);
            listView1.Columns.Add("Departament", 120, HorizontalAlignment.Center);
            PreluareDate();

        }

        private void PreluareDate()
        {
            List<Persoane> persoane = new List<Persoane>();
            DbProviderFactory factory = DbProviderFactories.GetFactory(ProviderName);
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = ConnectionString;
                connection.Open();

                DbCommand cmdSelect = connection.CreateCommand();
                cmdSelect.CommandText = "SELECT * FROM Persoane";
                using (DbDataReader reader = cmdSelect.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Persoane pers = new Persoane(reader.GetInt32(0), //id
                            reader.GetString(1),//nume
                            reader.GetString(2),//prenume
                            reader.GetDateTime(6),
                            reader.GetInt32(5),
                            reader.GetString(7),
                            reader.GetString(4),
                            reader.GetString(8));
                        persoane.Add(pers);
                    }
                }
            }
            //listView1.Items.Clear();
            foreach (Persoane p in persoane)
            {
                ListViewItem rand = new ListViewItem();
                rand.Text = p.Id.ToString(); //id
                rand.SubItems.Add(p.Nume); //nume
                rand.SubItems.Add(p.Prenume); //prenume
                rand.SubItems.Add(p.Nivel_studii); //nivel educatie
                rand.SubItems.Add(p.Data_angajare.ToString());//data angajarii
                rand.SubItems.Add(p.Departament);//departament
                listView1.Items.Add(rand);
            }
        }

        private void salveazăToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filename = "";
            SaveFileDialog sfd = new SaveFileDialog();

            sfd.Title = "Salvează lista angajaților";
            sfd.Filter = "Text File (.txt) | *.txt";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                filename = sfd.FileName.ToString();
                if (filename != "")
                {
                    using (StreamWriter sw = new StreamWriter(filename))
                    {
                        foreach (ListViewItem item in listView1.Items)
                        {
                            sw.WriteLine("{0}. {1} {2}, nivel educație - {3}, angajat {4}, departament {5}", item.SubItems[0].Text, item.SubItems[1].Text, item.SubItems[2].Text, item.SubItems[3].Text, item.SubItems[4].Text,item.SubItems[5].Text);
                        }
                    }
                }
            }
        }

        private void adaugăToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 form = new Form3();
            form.ShowDialog();
            listView1.Items.Clear();
            PreluareDate();
        }
    }
}
