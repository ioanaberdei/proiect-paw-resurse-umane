﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_PAW_Resurse_umane
{
    public partial class Form3 : Form
    {
        const string ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source= C:\Users\ioana\Desktop\AN 2\Sem 2\PAW\Proiect_PAW_Resurse_umane\Proiect_PAW_Resurse_umane\assets\bazadedate.mdb";
        const string ProviderName = @"System.Data.OleDb";
        public Form3()
        {
            InitializeComponent();
            cbDep.Items.Add("Administrativ");
            cbDep.Items.Add("Financiar");
            cbDep.Items.Add("IT");
            cbDep.Items.Add("Legal");

            cbEdu.Items.Add("primar");
            cbEdu.Items.Add("secundar");
            cbEdu.Items.Add("terțiar");

            cbSex.Items.Add("F");
            cbSex.Items.Add("M");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Validare();
        }

        private void Validare()
        {

            if (string.IsNullOrWhiteSpace(tbNume.Text))
            {
                errorProvider1.SetError(tbNume, "Introduceți numele.");
            }
            else if (string.IsNullOrWhiteSpace(tbPrenume.Text))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(tbPrenume, "Introduceti prenumele.");
            }
            else if (string.IsNullOrWhiteSpace(tbJob.Text))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(tbJob, "Introduceti id-ul jobului.");
            }
            else if (string.IsNullOrWhiteSpace(tbSal.Text))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(tbSal, "Introduceti salariul.");
            }
            else if (string.IsNullOrWhiteSpace(cbDep.Text))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(cbDep, "Selectați o valoare pentru departament.");
            }
            else if (string.IsNullOrWhiteSpace(cbEdu.Text))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(cbEdu, "Selectați o valoare pentru nivelul de studii.");
            }
            else if (string.IsNullOrWhiteSpace(cbSex.Text))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(cbSex, "Selectați sexul.");
            }
            else if (string.IsNullOrEmpty(monthCalendar1.SelectionRange.Start.ToShortDateString()))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(monthCalendar1, "Selectați data angajării.");
            }
            else
            {
                errorProvider1.Clear();
                DbProviderFactory factory = DbProviderFactories.GetFactory(ProviderName);
                using (DbConnection connection = factory.CreateConnection())
                {
                    connection.ConnectionString = ConnectionString;
                    connection.Open();

                    DbCommand select = connection.CreateCommand();
                    select.CommandText = "SELECT MAX(id_angajat) FROM Persoane";
                    int idMax = Convert.ToInt32(select.ExecuteScalar());

                    DbCommand cmdInsert = connection.CreateCommand();
                    cmdInsert.CommandText = "INSERT INTO Persoane(id_angajat,nume,prenume,sex,nivel_educatie,salariu,data_angajarii,departament,id_job) VALUES (@id,@nume ,@prenume, @sex, @niveleducatie, @salariu, @dataangajarii,@departament,@idjob)";

                    DbParameter paramId = cmdInsert.CreateParameter();
                    paramId.DbType = System.Data.DbType.Int32;
                    cmdInsert.Parameters.Add(paramId);

                    DbParameter paramNume = cmdInsert.CreateParameter();
                    paramNume.DbType = System.Data.DbType.String;
                    cmdInsert.Parameters.Add(paramNume);

                    DbParameter paramPrenume = cmdInsert.CreateParameter();
                    paramPrenume.DbType = System.Data.DbType.String;
                    cmdInsert.Parameters.Add(paramPrenume);

                    DbParameter paramSex = cmdInsert.CreateParameter();
                    paramSex.DbType = System.Data.DbType.String;
                    cmdInsert.Parameters.Add(paramSex);

                    DbParameter paramEdu = cmdInsert.CreateParameter();
                    paramEdu.DbType = System.Data.DbType.String;
                    cmdInsert.Parameters.Add(paramEdu);

                    DbParameter paramSal = cmdInsert.CreateParameter();
                    paramSal.DbType = System.Data.DbType.Int32;
                    cmdInsert.Parameters.Add(paramSal);

                    DbParameter paramData = cmdInsert.CreateParameter();
                    paramData.DbType = System.Data.DbType.DateTime;
                    cmdInsert.Parameters.Add(paramData);

                    DbParameter paramDep = cmdInsert.CreateParameter();
                    paramDep.DbType = System.Data.DbType.String;
                    cmdInsert.Parameters.Add(paramDep);

                    DbParameter paramJob = cmdInsert.CreateParameter();
                    paramJob.DbType = System.Data.DbType.String;
                    cmdInsert.Parameters.Add(paramJob);

                    paramId.Value = idMax + 1;
                    paramNume.Value = tbNume.Text;
                    paramPrenume.Value = tbPrenume.Text;
                    paramSex.Value = cbSex.Text;
                    paramEdu.Value = cbEdu.Text;
                    paramSal.Value = Int32.Parse(tbSal.Text);
                    paramData.Value = monthCalendar1.SelectionRange.Start;
                    paramDep.Value = cbDep.Text;
                    paramJob.Value = tbJob.Text;

                    cmdInsert.ExecuteNonQuery();
                }
                MessageBox.Show("Ați introdus un nou angajat în baza de date!");
            }
        }

        private void tbNume_TextChanged(object sender, EventArgs e)
        {
            if (!(new Regex(@"^[a-zA-Z]+$").IsMatch(tbNume.Text)))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(tbNume, "Introduceți doar litere.");
                tbNume.BackColor = Color.DarkRed;
                tbNume.ForeColor = Color.WhiteSmoke;
            }
            else
            {
                errorProvider1.Clear();
                tbNume.ForeColor = Color.Black;
                tbNume.BackColor = Color.White;
            }
        }

        private void tbPrenume_TextChanged(object sender, EventArgs e)
        {

            if (!(new Regex(@"^[a-zA-Z]+$").IsMatch(tbPrenume.Text)))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(tbPrenume, "Introduceți doar litere.");
                tbPrenume.BackColor = Color.DarkRed;
                tbPrenume.ForeColor = Color.WhiteSmoke;
            }
            else
            {
                errorProvider1.Clear();
                tbPrenume.ForeColor = Color.Black;
                tbPrenume.BackColor = Color.White;
            }
        }

        private void tbSal_TextChanged(object sender, EventArgs e)
        {
            if (!(new Regex(@"^\d{4}$").IsMatch(tbSal.Text)))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(tbSal, "Salariul trebuie să fie format din 4 cifre.");
                tbSal.BackColor = Color.DarkRed;
                tbSal.ForeColor = Color.WhiteSmoke;
            }
            else
            {
                errorProvider1.Clear();
                tbSal.ForeColor = Color.Black;
                tbSal.BackColor = Color.White;
            }
        }

        private void tbJob_TextChanged(object sender, EventArgs e)
        {
            if (!(new Regex(@"^\d{2}$").IsMatch(tbJob.Text)))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(tbJob, "Id-ul jobului trebuie să fie format din 2 cifre.");
                tbJob.BackColor = Color.DarkRed;
                tbJob.ForeColor = Color.WhiteSmoke;
            }
            else
            {
                errorProvider1.Clear();
                tbJob.ForeColor = Color.Black;
                tbJob.BackColor = Color.White;
            }
        }
    }
}
