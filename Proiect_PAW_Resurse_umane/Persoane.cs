﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect_PAW_Resurse_umane
{
    public class Persoane
    {
        private int id;
        private string nume;
        private string prenume;
        private DateTime data_angajare;
        private int salariu;
        private string departament;
        private string nivel_studii;
        private string id_job;

        public Persoane(int id, string nume, string prenume, DateTime data_angajare, int salariu, string departament, string nivel_studii, string id_job)
        {
            this.id = id;
            this.nume = nume;
            this.prenume = prenume;
            this.data_angajare = data_angajare;
            this.salariu = salariu;
            this.departament = departament;
            this.nivel_studii = nivel_studii;
            this.id_job = id_job;
        }

        public string Nume { get => nume; set => nume = value; }
        public string Prenume { get => prenume; set => prenume = value; }
        public DateTime Data_angajare { get => data_angajare; set => data_angajare = value; }
        public int Salariu { get => salariu; set => salariu = value; }
        public string Departament { get => departament; set => departament = value; }
        public string Nivel_studii { get => nivel_studii; set => nivel_studii = value; }
        public string Id_job { get => id_job; set => id_job = value; }
        public int Id { get => id; set => id = value; }
    }
    
    
}
