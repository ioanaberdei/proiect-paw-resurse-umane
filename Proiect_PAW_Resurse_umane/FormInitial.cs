﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_PAW_Resurse_umane
{
    public partial class FormInitial : Form
    {
        const string ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source= C:\Users\ioana\Desktop\AN 2\Sem 2\PAW\Proiect_PAW_Resurse_umane\Proiect_PAW_Resurse_umane\assets\bazadedate.mdb";
        const string ProviderName = @"System.Data.OleDb";

        public int ok = 0;

        public FormInitial()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(ProviderName);
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = ConnectionString;
                connection.Open();

                DbCommand cmdLogin = connection.CreateCommand();
                cmdLogin.CommandText = "SELECT * FROM utilizatori";
                using (DbDataReader reader = cmdLogin.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string numeUtilizator = reader.GetString(0);
                        string Password = reader.GetString(1);
                        if (textBox1.Text == numeUtilizator && textBox2.Text == Password)
                        {
                            MessageBox.Show("V-ati logat cu succes!");
                            this.Close();
                            ok = 1;
                        }
                        else
                        {
                            MessageBox.Show("Username-ul si parola nu corespund!");
                        }
                    }
                }
            }
        }
    }
}
