﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Common;

namespace Proiect_PAW_Resurse_umane
{

    public partial class UserControl1 : UserControl
    {
        const string ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source= C:\Users\ioana\Desktop\AN 2\Sem 2\PAW\Proiect_PAW_Resurse_umane\Proiect_PAW_Resurse_umane\assets\bazadedate.mdb";
        const string ProviderName = @"System.Data.OleDb";
        public UserControl1()
        {
            InitializeComponent();
            listView1.View = View.Details;
            listView1.Columns.Add("Id", 30, HorizontalAlignment.Center);
            listView1.Columns.Add("Nume", 120, HorizontalAlignment.Center);
            listView1.Columns.Add("Prenume", 120, HorizontalAlignment.Center);
            listView1.Columns.Add("Nivel educație", 110, HorizontalAlignment.Center);
            listView1.Columns.Add("Data angajării", 150, HorizontalAlignment.Center);
        }

        public List<Persoane> selectPers(List<Persoane> persoane, string tabela)
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(ProviderName);
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = ConnectionString;
                connection.Open();

                DbCommand cmdSelect = connection.CreateCommand();
                cmdSelect.CommandText = "SELECT * FROM Persoane WHERE departament = @tabela";
                DbParameter paramTabela = cmdSelect.CreateParameter();
                paramTabela.DbType = System.Data.DbType.String;
                cmdSelect.Parameters.Add(paramTabela);
                paramTabela.Value = tabela;

                using (DbDataReader reader = cmdSelect.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Persoane pers = new Persoane(reader.GetInt32(0),
                            reader.GetString(1),
                            reader.GetString(2),
                            reader.GetDateTime(6),
                            reader.GetInt32(5),
                            reader.GetString(7),
                            reader.GetString(4),
                            reader.GetString(8));
                        persoane.Add(pers);
                    }
                }
            }
            return persoane;
        }

        public void Afisare(List<Persoane> persoane)
        {
            listView1.Items.Clear();
            foreach (Persoane p in persoane)
            {
                ListViewItem rand = new ListViewItem();
                rand.Text = p.Id.ToString(); //id
                rand.SubItems.Add(p.Nume); //nume
                rand.SubItems.Add(p.Prenume); //prenume
                rand.SubItems.Add(p.Nivel_studii); //nivel educatie
                rand.SubItems.Add(p.Data_angajare.ToString());//data angajarii
                listView1.Items.Add(rand);
            }
        }


    }
}
