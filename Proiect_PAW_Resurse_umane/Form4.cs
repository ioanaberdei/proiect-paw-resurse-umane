﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_PAW_Resurse_umane
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
            userControl11.Show();
            userControl12.Hide();
            userControl13.Hide();
            userControl14.Hide();
        }

        private void userControl11_Load(object sender, EventArgs e)
        {
            List<Persoane> persoaneAdministrativ = new List<Persoane>();
            persoaneAdministrativ = userControl11.selectPers(persoaneAdministrativ, "Administrativ");
            userControl11.Afisare(persoaneAdministrativ);
        }

        private void userControl12_Load(object sender, EventArgs e)
        {
            List<Persoane> persoaneFinanciar = new List<Persoane>();
            persoaneFinanciar = userControl12.selectPers(persoaneFinanciar, "Financiar");
            userControl12.Afisare(persoaneFinanciar);
        }

        private void userControl13_Load(object sender, EventArgs e)
        {
            List<Persoane> persoaneIT = new List<Persoane>();
            persoaneIT = userControl13.selectPers(persoaneIT, "IT");
            userControl13.Afisare(persoaneIT);
        }

        private void userControl14_Load(object sender, EventArgs e)
        {
            List<Persoane> persoaneLegal = new List<Persoane>();
            persoaneLegal = userControl14.selectPers(persoaneLegal, "Legal");
            userControl14.Afisare(persoaneLegal);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            userControl11.Show();
            userControl12.Hide();
            userControl13.Hide();
            userControl14.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            userControl11.Hide();
            userControl12.Show();
            userControl13.Hide();
            userControl14.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            userControl11.Hide();
            userControl12.Hide();
            userControl13.Show();
            userControl14.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            userControl11.Hide();
            userControl12.Hide();
            userControl13.Hide();
            userControl14.Show();
        }
    }
}
