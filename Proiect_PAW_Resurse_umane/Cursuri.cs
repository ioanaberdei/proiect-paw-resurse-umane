﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect_PAW_Resurse_umane
{
    class Cursuri
    {
        private string denumire_curs;

        public Cursuri(string denumire_curs)
        {
            this.denumire_curs = denumire_curs;
        }

        public override string ToString()
        {
            return denumire_curs;
        }
    }
}
