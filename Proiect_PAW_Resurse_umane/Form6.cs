﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_PAW_Resurse_umane
{
    public partial class Form6 : Form
    {
        const string ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source= C:\Users\ioana\Desktop\AN 2\Sem 2\PAW\Proiect_PAW_Resurse_umane\Proiect_PAW_Resurse_umane\assets\bazadedate.mdb";
        const string ProviderName = @"System.Data.OleDb";
        private int nrPoz;
        private int nrNeg;
        private int nrTotal;
        public Form6()
        {
            InitializeComponent();
            textBox1.MouseDown += TextBox1_MouseDown;
            listBox1.DragDrop += ListBox1_DragDrop;
            listBox1.DragEnter += ListBox1_DragEnter;

            listBox2.DragDrop += ListBox2_DragDrop;
            listBox2.DragEnter += ListBox2_DragEnter;

            listBox3.DragDrop += ListBox3_DragDrop;
            listBox3.DragEnter += ListBox3_DragEnter;

            listBox4.DragDrop += ListBox4_DragDrop;
            listBox4.DragEnter += ListBox4_DragEnter;

            panel1.Paint += Panel1_Paint;

            getNr();
            afisareCursuri();

        }
        //drag&drop textbox
        private void TextBox1_MouseDown(object sender, MouseEventArgs e)
        {
            textBox1.DoDragDrop(textBox1.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }
        //grafic folosind clasa graphics
        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            int vl = 10, vr = e.ClipRectangle.Width - 20, vb = e.ClipRectangle.Height;
            float ponderePoz = (nrPoz * 100) / nrTotal;
            float pondereNeg = (nrNeg * 100) / nrTotal;
            float[] vy = new float[] { ponderePoz,pondereNeg };
            Brush[] vPens = new SolidBrush[]
            {
                new SolidBrush(Color.FromArgb(38,83,43)),
                new SolidBrush(Color.FromArgb(179, 57, 81))
            };

            int nrObs = 2; int lat = (vr - vl) / nrObs;
            for (int i = 0; i < nrObs; i++)
            {
                g.FillRectangle(vPens[i], vl + i * lat, vb - vy[i], lat, vy[i]);
            }
        }

        //functii drag&drop pentru listboxuri
        private void ListBox4_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(string)))
            {
                e.Effect = DragDropEffects.Copy;
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void ListBox4_DragDrop(object sender, DragEventArgs e)
        {
            string text = e.Data.GetData(typeof(string)).ToString();

            listBox4.Items.Add(text);
            if (e.Effect == DragDropEffects.Move)
            {
                textBox1.Clear();
            }
            Form7 form = new Form7();
            form.ShowDialog();
            string optiune = form.Optiune;
            inserareCurs("Reprezentant legal", text, optiune);
            Invalidate();
        }

        private void ListBox3_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(string)))
            {
                e.Effect = DragDropEffects.Copy;
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void ListBox3_DragDrop(object sender, DragEventArgs e)
        {
            string text = e.Data.GetData(typeof(string)).ToString();

            listBox3.Items.Add(text);
            if (e.Effect == DragDropEffects.Move)
            {
                textBox1.Clear();
            }
            Form7 form = new Form7();
            form.ShowDialog();
            string optiune = form.Optiune;
            inserareCurs("Contabil", text, optiune);
            Invalidate();
        }

        private void ListBox2_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(string)))
            {
                e.Effect = DragDropEffects.Copy;
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void ListBox2_DragDrop(object sender, DragEventArgs e)
        {
            string text = e.Data.GetData(typeof(string)).ToString();

            listBox2.Items.Add(text);
            if (e.Effect == DragDropEffects.Move)
            {
                textBox1.Clear();
            }
            Form7 form = new Form7();
            form.ShowDialog();
            string optiune = form.Optiune;
            inserareCurs("Backend developer", text, optiune);
            Invalidate();
        }

        private void ListBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(string)))
            {
                e.Effect = DragDropEffects.Copy;
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void ListBox1_DragDrop(object sender, DragEventArgs e)
        {
            string text = e.Data.GetData(typeof(string)).ToString();

            listBox1.Items.Add(text);
            if (e.Effect == DragDropEffects.Move)
            {
                textBox1.Clear();
            }
            Form7 form = new Form7();
            form.ShowDialog();
            string optiune = form.Optiune;
            inserareCurs("Frontend developer", text, optiune);

            Invalidate();
        }

        //functie de inserare cursuri in bd
        private void inserareCurs(string functie, string denumire, string feedback)
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(ProviderName);
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = ConnectionString;
                connection.Open();

                DbCommand select = connection.CreateCommand();
                select.CommandText = "SELECT MAX(id_curs) FROM Cursuri";
                int idMax = Convert.ToInt32(select.ExecuteScalar());

                DbCommand cmdInsert = connection.CreateCommand();
                cmdInsert.CommandText = "INSERT INTO Cursuri(id_curs,functie_destinata,denumire,feedback) VALUES (@id,@functie,@denumire,@feedback)";

                DbParameter paramId = cmdInsert.CreateParameter();
                paramId.DbType = System.Data.DbType.Int32;
                cmdInsert.Parameters.Add(paramId);

                DbParameter paramFct = cmdInsert.CreateParameter();
                paramFct.DbType = System.Data.DbType.String;
                cmdInsert.Parameters.Add(paramFct);

                DbParameter paramDen = cmdInsert.CreateParameter();
                paramDen.DbType = System.Data.DbType.String;
                cmdInsert.Parameters.Add(paramDen);

                DbParameter paramFeed = cmdInsert.CreateParameter();
                paramFeed.DbType = System.Data.DbType.String;
                cmdInsert.Parameters.Add(paramFeed);

                paramId.Value = idMax + 1;
                paramFct.Value = functie;
                paramDen.Value = denumire;
                paramFeed.Value = feedback;
                cmdInsert.ExecuteNonQuery();
            }
        }


        //calculare nr recenzii pozitive & negative ale cursurilor
        private void getNr()
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(ProviderName);
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = ConnectionString;
                connection.Open();

                DbCommand cmdSelectTot = connection.CreateCommand();
                cmdSelectTot.CommandText = "SELECT COUNT(id_curs) FROM Cursuri";
                nrTotal = Convert.ToInt32(cmdSelectTot.ExecuteScalar());

                DbCommand cmdSelectPoz = connection.CreateCommand();
                cmdSelectPoz.CommandText = "SELECT COUNT(id_curs) FROM Cursuri WHERE feedback = 'P'";
                nrPoz = Convert.ToInt32(cmdSelectPoz.ExecuteScalar());

                DbCommand cmdSelectNeg = connection.CreateCommand();
                cmdSelectNeg.CommandText = "SELECT COUNT(id_curs) FROM Cursuri WHERE feedback = 'N'";
                nrNeg = Convert.ToInt32(cmdSelectNeg.ExecuteScalar());
            }
        }

        //afisare cursuri in listboxuri in functie de job
        private void afisareCursuri()
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(ProviderName);
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = ConnectionString;
                connection.Open();

                DbCommand cmdSelectFront = connection.CreateCommand();
                cmdSelectFront.CommandText = "SELECT * FROM Cursuri WHERE functie_destinata = 'Frontend developer'";
                using (DbDataReader reader = cmdSelectFront.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Cursuri curs = new Cursuri(reader.GetString(2));
                        listBox1.Items.Add(curs);
                    }
                }

                DbCommand cmdSelectBack = connection.CreateCommand();
                cmdSelectBack.CommandText = "SELECT * FROM Cursuri WHERE functie_destinata = 'Backend developer'";
                using (DbDataReader reader = cmdSelectBack.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Cursuri curs = new Cursuri(reader.GetString(2));
                        listBox2.Items.Add(curs);
                    }
                }

                DbCommand cmdSelectLegal = connection.CreateCommand();
                cmdSelectLegal.CommandText = "SELECT * FROM Cursuri WHERE functie_destinata = 'Reprezentant legal'";
                using (DbDataReader reader = cmdSelectLegal.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Cursuri curs = new Cursuri(reader.GetString(2));
                        listBox4.Items.Add(curs);
                    }
                }

                DbCommand cmdSelectContabil = connection.CreateCommand();
                cmdSelectContabil.CommandText = "SELECT * FROM Cursuri WHERE functie_destinata = 'Contabil'";
                using (DbDataReader reader = cmdSelectContabil.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Cursuri curs = new Cursuri(reader.GetString(2));
                        listBox3.Items.Add(curs);
                    }
                }
            }
           
        }
    }
}
