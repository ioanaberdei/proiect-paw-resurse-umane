﻿namespace Proiect_PAW_Resurse_umane
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.userControl14 = new Proiect_PAW_Resurse_umane.UserControl1();
            this.userControl13 = new Proiect_PAW_Resurse_umane.UserControl1();
            this.userControl12 = new Proiect_PAW_Resurse_umane.UserControl1();
            this.userControl11 = new Proiect_PAW_Resurse_umane.UserControl1();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 320);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(790, 100);
            this.panel1.TabIndex = 2;
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button4.Location = new System.Drawing.Point(584, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(181, 107);
            this.button4.TabIndex = 3;
            this.button4.Text = "Legal";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button3.Location = new System.Drawing.Point(397, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(181, 107);
            this.button3.TabIndex = 2;
            this.button3.Text = "IT";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.Location = new System.Drawing.Point(210, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(181, 107);
            this.button2.TabIndex = 1;
            this.button2.Text = "Financiar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.Location = new System.Drawing.Point(23, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(181, 107);
            this.button1.TabIndex = 0;
            this.button1.Text = "Administrativ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // userControl14
            // 
            this.userControl14.Location = new System.Drawing.Point(139, 39);
            this.userControl14.Margin = new System.Windows.Forms.Padding(0);
            this.userControl14.Name = "userControl14";
            this.userControl14.Size = new System.Drawing.Size(534, 244);
            this.userControl14.TabIndex = 7;
            this.userControl14.Load += new System.EventHandler(this.userControl14_Load);
            // 
            // userControl13
            // 
            this.userControl13.Location = new System.Drawing.Point(139, 39);
            this.userControl13.Margin = new System.Windows.Forms.Padding(0);
            this.userControl13.Name = "userControl13";
            this.userControl13.Size = new System.Drawing.Size(534, 244);
            this.userControl13.TabIndex = 6;
            this.userControl13.Load += new System.EventHandler(this.userControl13_Load);
            // 
            // userControl12
            // 
            this.userControl12.Location = new System.Drawing.Point(139, 39);
            this.userControl12.Margin = new System.Windows.Forms.Padding(0);
            this.userControl12.Name = "userControl12";
            this.userControl12.Size = new System.Drawing.Size(534, 244);
            this.userControl12.TabIndex = 5;
            this.userControl12.Load += new System.EventHandler(this.userControl12_Load);
            // 
            // userControl11
            // 
            this.userControl11.Location = new System.Drawing.Point(139, 39);
            this.userControl11.Margin = new System.Windows.Forms.Padding(0);
            this.userControl11.Name = "userControl11";
            this.userControl11.Size = new System.Drawing.Size(534, 244);
            this.userControl11.TabIndex = 4;
            this.userControl11.Load += new System.EventHandler(this.userControl11_Load);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(19)))), ((int)(((byte)(22)))));
            this.ClientSize = new System.Drawing.Size(790, 420);
            this.Controls.Add(this.userControl13);
            this.Controls.Add(this.userControl11);
            this.Controls.Add(this.userControl14);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.userControl12);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form4";
            this.Text = "Departamente";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private UserControl1 userControl11;
        private UserControl1 userControl12;
        private UserControl1 userControl13;
        private UserControl1 userControl14;
    }
}