﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_PAW_Resurse_umane
{
    public partial class Form5 : Form
    {
        const string ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source= C:\Users\ioana\Desktop\AN 2\Sem 2\PAW\Proiect_PAW_Resurse_umane\Proiect_PAW_Resurse_umane\assets\bazadedate.mdb";
        const string ProviderName = @"System.Data.OleDb";
        private int nrTotal;
        private int nrFemei;
        private int nrMasc;
        private int nrPrimar;
        private int nrSecundar;
        private int nrTertiar;
        private int nrAdmin;
        private int nrFin;
        private int nrLeg;
        private int nrIT;

        public Form5()
        {
            InitializeComponent();

            getDataRepartSex();
           
            float pondereMasc = (nrMasc * 100) / nrTotal;
            float pondereFem = (nrFemei * 100) / nrTotal;

            chart1.Series["Series1"].Points.AddXY("Feminin", pondereFem);
            chart1.Series["Series1"].Points[0].Color = Color.FromArgb(130, 102, 127);
            chart1.Series["Series1"].Points.AddXY("Masculin", pondereMasc);
            chart1.Series["Series1"].Points[1].Color = Color.FromArgb(25, 29, 50);

            getDataRepartEdu();
            float ponderePrim = (nrPrimar * 100) / nrTotal;
            float pondereSec = (nrSecundar * 100) / nrTotal;
            float pondereTert = (nrTertiar * 100) / nrTotal;

            chart2.Series["Series1"].Points.AddXY("Primar", ponderePrim);
            chart2.Series["Series1"].Points[0].Color = Color.FromArgb(112, 117, 147);
            chart2.Series["Series1"].Points.AddXY("Secundar", pondereSec);
            chart2.Series["Series1"].Points[1].Color = Color.FromArgb(51, 59, 101);
            chart2.Series["Series1"].Points.AddXY("Tertiar", pondereTert);
            chart2.Series["Series1"].Points[2].Color = Color.FromArgb(25, 29, 50);


            getDataRepartDep();
            float pondereAdmin = (nrAdmin * 100) / nrTotal;
            float pondereLegal = (nrLeg * 100) / nrTotal;
            float pondereIT = (nrIT * 100) / nrTotal;
            float pondereFin = (nrFin * 100) / nrTotal;
            chart3.Series["Series1"].Points.AddXY("Administrativ", pondereAdmin);
            chart3.Series["Series1"].Points[0].Color = Color.FromArgb(57, 48, 74);
            chart3.Series["Series1"].Points.AddXY("Financiar", pondereFin);
            chart3.Series["Series1"].Points[1].Color = Color.FromArgb(130, 102, 127);
            chart3.Series["Series1"].Points.AddXY("IT", pondereIT);
            chart3.Series["Series1"].Points[2].Color = Color.FromArgb(25, 29, 50);
            chart3.Series["Series1"].Points.AddXY("Legal", pondereLegal);
            chart3.Series["Series1"].Points[3].Color = Color.FromArgb(90, 125, 124);
        }

        private void getDataRepartSex()
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(ProviderName);
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = ConnectionString;
                connection.Open();

                DbCommand cmdSelectTotal = connection.CreateCommand();
                cmdSelectTotal.CommandText = "SELECT COUNT(id_angajat) FROM Persoane";
                nrTotal = Convert.ToInt32(cmdSelectTotal.ExecuteScalar());

                DbCommand cmdSelectFem = connection.CreateCommand();
                cmdSelectFem.CommandText = "SELECT COUNT(id_angajat) FROM Persoane WHERE sex = 'F'";
                nrFemei = Convert.ToInt32(cmdSelectFem.ExecuteScalar());

                DbCommand cmdSelectMasc = connection.CreateCommand();
                cmdSelectMasc.CommandText = "SELECT COUNT(id_angajat) FROM Persoane WHERE sex = 'M'";
                nrMasc = Convert.ToInt32(cmdSelectMasc.ExecuteScalar());
            }
        }

        private void getDataRepartEdu()
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(ProviderName);
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = ConnectionString;
                connection.Open();

                DbCommand cmdSelectPrimar = connection.CreateCommand();
                cmdSelectPrimar.CommandText = "SELECT COUNT(id_angajat) FROM Persoane WHERE nivel_educatie = 'primar'";
                nrPrimar = Convert.ToInt32(cmdSelectPrimar.ExecuteScalar());

                DbCommand cmdSelectSec = connection.CreateCommand();
                cmdSelectSec.CommandText = "SELECT COUNT(id_angajat) FROM Persoane WHERE nivel_educatie = 'secundar'";
                nrSecundar = Convert.ToInt32(cmdSelectSec.ExecuteScalar());

                DbCommand cmdSelectTert = connection.CreateCommand();
                cmdSelectTert.CommandText = "SELECT COUNT(id_angajat) FROM Persoane WHERE nivel_educatie = 'terțiar'";
                nrTertiar = Convert.ToInt32(cmdSelectTert.ExecuteScalar());
            }
        }

        private void getDataRepartDep()
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(ProviderName);
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = ConnectionString;
                connection.Open();

                DbCommand cmdSelectAdmin = connection.CreateCommand();
                cmdSelectAdmin.CommandText = "SELECT COUNT(id_angajat) FROM Persoane WHERE departament = 'Administrativ'";
                nrAdmin = Convert.ToInt32(cmdSelectAdmin.ExecuteScalar());

                DbCommand cmdSelectFin = connection.CreateCommand();
                cmdSelectFin.CommandText = "SELECT COUNT(id_angajat) FROM Persoane WHERE departament = 'Financiar'";
                nrFin = Convert.ToInt32(cmdSelectFin.ExecuteScalar());

                DbCommand cmdSelectIT = connection.CreateCommand();
                cmdSelectIT.CommandText = "SELECT COUNT(id_angajat) FROM Persoane WHERE departament = 'IT'";
                nrIT = Convert.ToInt32(cmdSelectIT.ExecuteScalar());

                DbCommand cmdSelectLegal = connection.CreateCommand();
                cmdSelectLegal.CommandText = "SELECT COUNT(id_angajat) FROM Persoane WHERE departament = 'Legal'";
                nrLeg = Convert.ToInt32(cmdSelectLegal.ExecuteScalar());
            }
        }
        /*handler pentru printare*/
        private void doc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Bitmap bmp = new Bitmap(panel1.Width, panel1.Height, panel1.CreateGraphics());
            panel1.DrawToBitmap(bmp, new Rectangle(0, 0, panel1.Width, panel1.Height));
            RectangleF bounds = e.PageSettings.PrintableArea;
            float factor = ((float)bmp.Height / (float)bmp.Width);
            e.Graphics.DrawImage(bmp, bounds.Left, bounds.Top, bounds.Width, factor * bounds.Width);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Drawing.Printing.PrintDocument doc = new System.Drawing.Printing.PrintDocument();
            doc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(doc_PrintPage);
            PrintPreviewDialog ppd = new PrintPreviewDialog();
            ppd.Document = doc;
            ppd.ShowDialog();
        }
    }
}
